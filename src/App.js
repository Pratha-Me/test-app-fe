import React from 'react';
import { useDispatch } from 'react-redux'
import {
  Route,
  Switch,
  BrowserRouter as Router,
  Redirect,
} from "react-router-dom";
import routes from "./routes/routes";
import { getAuthUser, isUserAuthenticated } from "./helpers/AuthHelpers";
import { loginUserSuccessful } from "./redux/auth/login/actions";

function App() {

  const dispatch = useDispatch();

  if (getAuthUser()) {
    const user = getAuthUser();
    dispatch(loginUserSuccessful(user))
  }

  const PrivateRoute = ({ component: Component, role: Role, ...rest }) => (
    <Route
      {...rest}
      render={() => {
        if (isUserAuthenticated() !== true) {
          return <Redirect to='/logout' />;
        }

        if (Role) {
          const user = getAuthUser();
          if (Role.includes(user.authorities[0])) {
            return <Component />;
          } else {
            return <Redirect to='/unauthorized' />;
          }
        } else {
          return <Component />;
        }
      }}
    />
  );

  return (
    <React.Fragment>
      <Router>
        <Switch>
          {routes.map((route, idx) =>
            route.isPrivate ? (
              <PrivateRoute
                path={route.path}
                component={route.component}
                role={route.role}
                key={idx}
              />
            ) : route.path === "/" ? (
              <Route
                path={route.path}
                exact
                component={route.component}
                key={idx}
              />
            ) : (
              <Route path={route.path} component={route.component} key={idx} />
            )
          )}
        </Switch>
      </Router>
    </React.Fragment>
  );
}

export default App;
