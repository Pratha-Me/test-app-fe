import { useForm, ValidationRule } from "react-hook-form";

const index = () => {
    const { register, handleSubmit, errors } = useForm();

    const handleSubmitForm = async (formData) => {
        console.log(formData);

        // await props.postLogin(formData, props.history)
        // setLoginError(true)
    };

    return (
        <div className="flex justify-center content-start items-start">
            <form onSubmit={handleSubmit(handleSubmitForm)}>
                <label htmlFor="fname">Full Name</label>
                <input type="text" id="name" placeholder="Your full name..." {...register("name", { required: true })} />
                {errors.name && (
                    <p className="text-danger ml-3"> * Name is required!</p>
                )}
                <label htmlFor="lname">Email</label>
                <input type="email" id="email" placeholder="Your email ID..." {...register("email", { required: true })} />
                {errors.email && (
                    <p className="text-danger ml-3"> * Email is required!</p>
                )}

                <label htmlFor="lname">Phone</label>
                <input type="phone" id="phone" placeholder="Your phone number..." {...register("email", { required: true, pattern="^[0-9]+$" })} />
                {errors.phone && (
                    <p className="text-danger ml-3"> * Phone is required!</p>
                )}

                <input type="submit" value="Submit" />
            </form>
        </div>
    )
}

export default index
